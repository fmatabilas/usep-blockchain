PK
     t�N���       package.json{"engines":{"composer":"^0.20.5"},"name":"cashless-network","version":"0.0.1","description":"Cashless Network","scripts":{"prepublish":"mkdirp ./dist && composer archive create --sourceType dir --sourceName . -a ./dist/cashless-network.bna","pretest":"npm run lint","lint":"eslint .","test":"nyc mocha -t 0 test/*.js && cucumber-js"},"keywords":["composer","composer-network"],"author":"Floyd","email":"fmatabilas@maersummit.tech","license":"Apache-2.0","devDependencies":{"composer-admin":"^0.20.5","composer-cli":"^0.20.5","composer-client":"^0.20.5","composer-common":"^0.20.5","composer-connector-embedded":"^0.20.5","composer-cucumber-steps":"^0.20.5","chai":"latest","chai-as-promised":"latest","cucumber":"^2.2.0","eslint":"latest","nyc":"latest","mkdirp":"latest","mocha":"latest"}}PK
     t�N"�E�%   %   	   README.md# cashless-network

Cashless Network
PK
     t�N%���  �     permissions.acl/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

rule NetworkAdminUser {
    description: "Grant business network administrators full access to user resources"
    participant: "org.hyperledger.composer.system.NetworkAdmin"
    operation: ALL
    resource: "**"
    action: ALLOW
}

rule Participants_Rule1 {
  description: "Participantss should be able to see the history of their own transactions only"
  participant(p): "org.hyperledger.composer.system.Participant"
  operation: READ
  resource(r): "org.hyperledger.composer.system.HistorianRecord"
  condition: (r.participantInvoking.getIdentifier() != p.getIdentifier())
  action: DENY
}

rule SystemACL {
    description: "Grant everyone full access to system resources"
    participant: "ANY"
    operation: ALL
    resource: "org.hyperledger.composer.system.**"
    action: ALLOW
}

rule Official_Rule1 {
  description: "Allow officials to have access on their own information only"
  participant(p): "cashless.participants.Official"
  operation: READ, UPDATE
  resource(r): "cashless.participants.Official"
  condition: (p.getIdentifier() == r.getIdentifier())
  action: ALLOW
}

rule Official_Rule2 {
  description: "Allow officials to have access on participant person"
  participant: "cashless.participants.Official"
  operation: ALL
  resource: "cashless.participants.Person"
  action: ALLOW
}

rule Official_Rule3 {
  description: "Allow officials to have access on asset wallet"
  participant: "cashless.participants.Official"
  operation: ALL
  resource: "cashless.assets.Wallet"
  action: ALLOW
}

rule Official_Rule4 {
  description: "Allow officials to have access on transaction 'update wallet'"
  participant: "cashless.participants.Official"
  operation: ALL
  resource: "cashless.transactions.UpdateWallet"
  action: ALLOW
}

rule Official_Rule5 {
  description: "Allow officials to have access on transaction 'create person'"
  participant: "cashless.participants.Official"
  operation: ALL
  resource: "cashless.transactions.CreatePerson"
  action: ALLOW
}

rule Official_Rule6 {
  description: "Allow officials to have access on transaction 'sell'"
  participant: "cashless.participants.Official"
  operation: ALL
  resource: "cashless.transactions.Sell"
  action: ALLOW
}

rule Person_Rule1 {
  description: "Description of the Transactional ACL rule"
  participant(p): "cashless.participants.Person"
  operation: READ, UPDATE
  resource(r): "cashless.assets.Wallet"
  transaction(tx): "cashless.transactions.UpdateWallet"
  condition: (p.type == 'VENDOR')
  action: ALLOW
}PK
     t�N               models/PK
     t�N=��A�   �      models/cashless.assets.ctonamespace cashless.assets
import cashless.participants.*

asset Wallet identified by walletId{
  o String walletId
  o Integer value default=0
  --> Person owner
}PK
     t�Ne����   �      models/cashless.base.ctonamespace cashless.base

abstract participant PersonalInfo {
  o String firstName
  o String middleName
  o String lastName
  o Gender gender
}

enum Gender {
  o MALE
  o FEMALE
}

enum Type {
  o STANDARD
  o VENDOR
}

enum Action {
  o ADD
  o DEDUCT
}PK
     t�N0x�         models/cashless.events.ctonamespace cashless.events

PK
     t�N9B`E#  #      models/cashless.participants.ctonamespace cashless.participants
import cashless.assets.*
import cashless.base.*

participant Official identified by officialId {
  o String officialId
  o String name
}

participant Person identified by personId extends PersonalInfo {
  o String personId
  o Type type
  --> Wallet wallet
}
PK
     t�N�&�  �      models/cashless.transactions.ctonamespace cashless.transactions
import cashless.assets.*
import cashless.base.*
import cashless.participants.*

transaction UpdateWallet {
  --> Person person
  o Integer value
  o Action action
}

transaction CreatePerson {
  o String firstName
  o String middleName
  o String lastName
  o Gender gender
  o Type type optional
}

transaction Sell {
  --> Person seller
  --> Person buyer
  o Integer value
  o String[] particulars
}PK
     t�N               lib/PK
     t�N�6sJ
  J
     lib/transactions.js'use strict';

/**
 * Invoke update wallet transaction
 * @param {cashless.transactions.UpdateWallet} update - the update to be processed
 * @transaction
 */
async function updateWallet(update) {

    let assetRegistry = await getAssetRegistry('cashless.assets.Wallet');
    let wallet = update.person.wallet;
    var operators = {
        'ADD': function(oldValue, newValue) { return oldValue + newValue },
        'DEDUCT': function(oldValue, newValue) { return oldValue - newValue },
    };
    
    wallet.value = operators[update.action](wallet.value, update.value);
    
    if(wallet.value > 2000){
        throw new Error('Wallet limit is 2000!');
    }

    await assetRegistry.update(wallet);
}

/**
 * Invoke create person transaction
 * @param {cashless.transactions.CreatePerson} create - the create to be processed
 * @transaction
 */
async function createPerson(create) {

    // Get the factory.
    var factory = getFactory();
    let assetRegistry = await getAssetRegistry('cashless.assets.Wallet');
    let participantRegistry = await getParticipantRegistry('cashless.participants.Person');

    var id = Date.now().toString(36) + Math.random().toString(36).substr(2, 9);

    // Create a new person
    var person = factory.newResource('cashless.participants', 'Person', 'PERSON_' + id);
    // Create a new wallet
    var wallet = factory.newResource('cashless.assets', 'Wallet', 'WALLET_' + id);

    // insert person data
    person.firstName = create.firstName;
    person.middleName = create.middleName;
    person.lastName = create.lastName;
    person.gender = create.gender;
    person.type = (typeof create.type === 'undefined') ? 'STANDARD' : create.type;
    person.wallet = wallet;

    // insert wallet data
    wallet.owner = person;

    // persist the state of the generated asset and participant
    await participantRegistry.add(person);
    await assetRegistry.add(wallet);
}

/**
 * Invoke selling transaction
 * @param {cashless.transactions.Sell} sell - the selling to be processed
 * @transaction
 */
async function sell(sell) {

    if(sell.seller.type != 'VENDOR'){
        throw new Error('Seller is not authorized!');
    }

    let assetRegistry = await getAssetRegistry('cashless.assets.Wallet');
    let buyerWallet = sell.buyer.wallet;
    let sellerWallet = sell.seller.wallet;
    
    buyerWallet.value = buyerWallet.value - sell.value;
    sellerWallet.value = sellerWallet.value + sell.value;
    
    if(buyerWallet.value < 0){
        throw new Error('Insufficient balance!');
    }

    await assetRegistry.update(buyerWallet);
    await assetRegistry.update(sellerWallet);
}PK 
     t�N���                     package.jsonPK 
     t�N"�E�%   %   	             @  README.mdPK 
     t�N%���  �               �  permissions.aclPK 
     t�N                        �  models/PK 
     t�N=��A�   �                �  models/cashless.assets.ctoPK 
     t�Ne����   �                �  models/cashless.base.ctoPK 
     t�N0x�                   �  models/cashless.events.ctoPK 
     t�N9B`E#  #                6  models/cashless.participants.ctoPK 
     t�N�&�  �                �  models/cashless.transactions.ctoPK 
     t�N                        �  lib/PK 
     t�N�6sJ
  J
               �  lib/transactions.jsPK      �  $     